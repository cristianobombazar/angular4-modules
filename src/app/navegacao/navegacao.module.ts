import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import {BotoesModule} from '../botoes/botoes.module';
import { LinhaComponent } from './linha/linha.component';

@NgModule({
  imports: [
    CommonModule,
    BotoesModule
  ],
  declarations: [MenuComponent, LinhaComponent],
  // REEXPORTA O BOTOESMODULE. QUEM IMPORTAR O NAVEGACAOMODULA, VAI IMPORTAR TAMBÉM O BOTOESMODULE.
  exports: [MenuComponent, BotoesModule]
})
export class NavegacaoModule { }
