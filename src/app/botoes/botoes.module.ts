import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BotaoGrandeComponent } from './botao-grande/botao-grande.component';

@NgModule({
  // QUANDO É CRIADO SUB MODULOS É PRECISO IMPORTAR O COMMON MODULE PARA UTILIZAR NGIF OU NGFOR E ETC.
  imports: [
    CommonModule
  ],
  declarations:  [BotaoGrandeComponent],
  // EXPORTA O BOTAO GRANDDE PARA OUTROS MÓDULOS UTILIZAREM O BOTAO GRANDE
  exports: [BotaoGrandeComponent]
})
export class BotoesModule { }
